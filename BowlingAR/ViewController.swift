//
//  ViewController.swift
//  BowlingAR
//
//  Created by Akash Bakshi on 2018-06-04.
//  Copyright © 2018 Akash Bakshi. All rights reserved.
//

import UIKit
import SceneKit
import ARKit

class ViewController: UIViewController, ARSCNViewDelegate {

    @IBOutlet var sceneView: ARSCNView!
    
    var bowlingBall: SCNNode!
    var dummyBall: SCNNode!
    var bowlingPins: [SCNNode:Int] = [:]
    var bowlingPinHit: [SCNNode] = []
    
    var hasLaneBeenAnchored = false
    var ballBeenBowled = false
    var collisionHasHappened = false
    
    var scenePos:SCNVector3!
    
    var placeLaneBtn: UIButton!
    var instructionsLabel: UILabel!
    
    var startingPinPosition: SCNVector3!
    var startingBallPosition: SCNMatrix4!
    
    var addLaneBtn: UIButton!
    
    enum bodyType: Int{
        case fivePin = 1
        case left3Pin = 2
        case right3Pin = 4
        case left2Pin = 8
        case right2Pin = 16
        case bowlingBall = 32
        case bowlingHead = 64
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Set the view's delegate
        sceneView.delegate = self
        
        // Show statistics such as fps and timing information
        sceneView.showsStatistics = true
        sceneView.debugOptions = [ARSCNDebugOptions.showFeaturePoints,
                                  ARSCNDebugOptions.showWorldOrigin/*,
             .showBoundingBoxes,
             .showWireframe,
             .showSkeletons,
             .showPhysicsShapes,
             .showCameras*/
        ]
        // Create a new scene
       
        
        // Set the scene to the view
        sceneView.scene = SCNScene()
        
        laneBtnToView()
        
        /*
        let centerx = NSLayoutConstraint(item: addLaneBtn, attribute: .centerX, relatedBy: .equal, toItem: view, attribute: .centerX, multiplier: 1, constant: 0)
        view.addConstraint(centerx)
        
        let centery = NSLayoutConstraint(item: addLaneBtn, attribute: .centerY, relatedBy: .equal, toItem: view, attribute: .centerY, multiplier: 1, constant: 0)
        view.addConstraint(centery)
 */
        
        
    }
    
    @objc func PlaceBowlingLane(){
        
    }
    
    func laneBtnToView(){
        
        addLaneBtn = UIButton()
        view.addSubview(addLaneBtn)
        addLaneBtn.translatesAutoresizingMaskIntoConstraints = false
        addLaneBtn.setTitle("Place Lane",for: .normal)
        addLaneBtn.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 0.0).isActive = true
        addLaneBtn.centerXAnchor.constraint(equalTo: view.centerXAnchor, constant: 0.0).isActive = true
        addLaneBtn.addTarget(self, action: #selector(PlaceBowlingLane), for: .touchUpInside)

        addLaneBtn.isHidden = true
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // Create a session configuration
        let configuration = ARWorldTrackingConfiguration()
        configuration.planeDetection = .horizontal
        // Run the view's session
        sceneView.session.run(configuration)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        // Pause the view's session
        sceneView.session.pause()
    }

    // MARK: - ARSCNViewDelegate
    
/*
    // Override to create and configure nodes for anchors added to the view's session.
    func renderer(_ renderer: SCNSceneRenderer, nodeFor anchor: ARAnchor) -> SCNNode? {
        let node = SCNNode()
     
        return node
    }
*/
    
    func renderer(_ renderer: SCNSceneRenderer, didAdd node: SCNNode, for anchor: ARAnchor) {
        guard let planeAnchor = anchor as? ARPlaneAnchor else {return}
        
        print("found plane: \(planeAnchor)")
        
        if(!hasLaneBeenAnchored){
            let plane = SCNPlane(width: CGFloat(planeAnchor.extent.x), height: CGFloat(planeAnchor.extent.z))
            let planeNode = SCNNode(geometry: plane)
            planeNode.simdPosition = float3(planeAnchor.center.x, 0, planeAnchor.center.z)
            
            planeNode.eulerAngles.x = -.pi / 2
            
            planeNode.opacity = 0.25
            
            planeNode.geometry?.materials.first?.diffuse.contents = UIColor.init(red: 255.0/255.0, green: 146.0/255.0, blue: 237.0/255.0, alpha: 1.0)
            planeNode.position = SCNVector3(planeAnchor.center.x,planeAnchor.center.y,planeAnchor.center.z)
            
            node.addChildNode(planeNode)
            addLaneBtn.isHidden = false
        }
        
    }
    
    func renderer(_ renderer: SCNSceneRenderer, didUpdate node: SCNNode, for anchor: ARAnchor) {
        guard anchor is ARPlaneAnchor else { return }
        // remove existing plane nodes
       
        if(hasLaneBeenAnchored){
            node.enumerateChildNodes { (childNode, _) in
                childNode.removeFromParentNode()
                
            }
        }
    }
    
    func session(_ session: ARSession, didFailWithError error: Error) {
        // Present an error message to the user
        
    }
    
    func sessionWasInterrupted(_ session: ARSession) {
        // Inform the user that the session has been interrupted, for example, by presenting an overlay
        
    }
    
    func sessionInterruptionEnded(_ session: ARSession) {
        // Reset tracking and/or remove existing anchors if consistent tracking is required
        
    }
}
